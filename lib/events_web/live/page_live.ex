defmodule EventsWeb.PageLive do
  use EventsWeb, :live_view

  alias Events.Accounts

  @impl true
  def mount(_params, session, socket) do
    IO.puts inspect session
    IO.puts session[:user_id]

    socket =
      case session[:user_id] do
        nil ->
          IO.puts("1")
          assign(socket, current_user: nil)
        id ->
          IO.puts("2")
          user = Accounts.get_user(id)
          assign(socket, current_user: user)
      end

    socket =
      socket
      |> assign(query: "", results: %{})

    {:ok, socket}
  end

  @impl true
  def handle_event("suggest", %{"q" => query}, socket) do
    {:noreply, assign(socket, results: search(query), query: query)}
  end

  @impl true
  def handle_event("search", %{"q" => query}, socket) do
    case search(query) do
      %{^query => vsn} ->
        {:noreply, redirect(socket, external: "https://hexdocs.pm/#{query}/#{vsn}")}

      _ ->
        {:noreply,
         socket
         |> put_flash(:error, "No dependencies found matching \"#{query}\"")
         |> assign(results: %{}, query: query)}
    end
  end

  defp search(query) do
    if not EventsWeb.Endpoint.config(:code_reloader) do
      raise "action disabled when not in development"
    end

    for {app, desc, vsn} <- Application.started_applications(),
        app = to_string(app),
        String.starts_with?(app, query) and not List.starts_with?(desc, ~c"ERTS"),
        into: %{},
        do: {app, vsn}
  end
end
