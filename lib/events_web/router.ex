defmodule EventsWeb.Router do
  use EventsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {EventsWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      forward "/sent_emails", Bamboo.SentEmailViewerPlug
      live_dashboard "/dashboard", metrics: EventsWeb.Telemetry
    end
  end

  scope "/", EventsWeb do
    pipe_through :browser

    live "/", PageLive, :index

    get "/auth", AuthController, :new
    get "/auth/verify", AuthController, :verify
    get "/auth/logout", AuthController, :logout
    post "/auth/login", AuthController, :login
  end
end
