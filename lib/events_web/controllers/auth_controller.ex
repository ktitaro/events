defmodule EventsWeb.AuthController do
  use EventsWeb, :controller

  alias Services.Guardian.Passwordless

  @backurl_key :backurl

  def new(conn, params) do
    case get_session(conn, :current_user) do
      nil ->
        action = Routes.auth_path(conn, :login)
        render(conn, "new.html", action: action)
      _user ->
        redirect_maybe_backurl(conn, params)
    end
  end

  def login(conn, %{"email" => email} = params) do
    base_url = Routes.auth_url(conn, :verify)
    base_url =
      case params[@backurl_key] do
        nil -> base_url
        backurl -> "#{base_url}?backurl=#{backurl}&"
      end
    Passwordless.execute_flow(email, base_url)
    conn
    |> put_flash(:info, "Check your email")
    |> redirect(to: "/")
  end

  def verify(conn, %{"token" => token} = params) do
    case Passwordless.verify_flow(token) do
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> put_flash(:info, "Welcome back!")
        |> redirect_maybe_backurl(params)
      {:error, :expired} ->
        conn
        |> put_flash(:info, "Authorization link has expired")
        |> put_flash(:info, "Fill the form for another one")
        |> redirect_with_backurl(Routes.auth_path(conn, :new), params)
      {:error, _reason} ->
        conn
        |> redirect(to: "/")
    end
  end

  def logout(conn, _params) do
    case get_session(conn, :user_id) do
      nil ->
        conn
        |> redirect(to: "/")
      _user ->
        conn
        |> clear_session()
        |> put_flash(:info, "You now logged out")
        |> redirect(to: "/")
    end
  end

  defp redirect_with_backurl(conn, to, params) do
    case params[@backurl_key] do
      nil -> redirect(conn, to: to)
      backurl -> redirect(conn, to: to, backurl: backurl)
    end
  end

  defp redirect_maybe_backurl(conn, params) do
    case params[@backurl_key] do
      nil -> redirect(conn, to: "/")
      backurl -> redirect(conn, to: backurl)
    end
  end
end
