defmodule Services.Postman.Template do
  use Bamboo.Phoenix, view: EventsWeb.EmailView

  def base_email do
    new_email()
    |> from("noreply@wiz.ge")
    |> put_html_layout({EventsWeb.LayoutView, "email.html"})
  end

  def welcome(email) do
    base_email()
    |> to(email)
    |> subject("Welcome!")
    |> render(:welcome)
  end

  def magic_link(user, callback_url) do
    base_email()
    |> assign(:user, user)
    |> assign(:callback_url, callback_url)
    |> to(user.email)
    |> subject("Authorization link")
    |> render("magic_link.html")
  end
end
