defmodule Services.Guardian.Token do
  alias Phoenix.Token
  alias EventsWeb.Endpoint

  @auth_token_lifetime 30 * 60

  def generate(id) do
    salt = Application.get_env(:events, Endpoint)[:secret_key_base]
    token = Token.sign(Endpoint, salt, id)
    token
  end

  def verify(token) do
    salt = Application.get_env(:events, Endpoint)[:secret_key_base]
    Token.verify(Endpoint, salt, token, max_age: @auth_token_lifetime)
  end
end
