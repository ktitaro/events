defmodule Services.Guardian.Passwordless do
  alias Events.Accounts
  alias Services.Postman.Mailer
  alias Services.Postman.Template
  alias Services.Guardian.Token

  def send_magic_link(email, callback_url) do
    Template.magic_link(email, callback_url)
    |> Mailer.deliver_now()
  end

  def execute_flow(email, base_url) do
    {:ok, user} = Accounts.get_user_by_email_or_create(email)
    token = Token.generate(user.id)
    callback_url = "#{base_url}?token=#{token}"
    send_magic_link(user, callback_url)
  end

  def verify_flow(token) do
    case Token.verify(token) do
      {:ok, id} ->
        case Accounts.get_user(id) do
          nil -> {:error, :unknown_user}
          user -> {:ok, user}
        end
      {:error, reason} -> {:error, reason}
    end
  end
end
