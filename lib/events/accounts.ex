defmodule Events.Accounts do
  import Ecto.Query, warn: false

  alias Events.Repo
  alias Events.Accounts.User

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def get_user(id) do
    Repo.get_by(User, %{id: id})
  end

  def get_user_by_email(email) do
    User
    |> where(email: ^email)
    |> Repo.one
  end

  def get_user_by_email_or_create(email) do
    case get_user_by_email(email) do
      nil -> create_user(%{email: email})
      user -> {:ok, user}
    end
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end
end
