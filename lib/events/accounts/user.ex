defmodule Events.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :deleted_at, :utc_datetime
    field :email, :string
    field :fullname, :string
    field :is_deleted, :boolean, default: false
    field :is_online, :boolean, default: false
    field :last_login, :utc_datetime
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_required([:email])
    |> unique_constraint(:email)
  end
end
