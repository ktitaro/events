import 'bootstrap'
import 'phoenix_html'

import NProgress from 'nprogress'
import { Socket } from 'phoenix'
import { LiveSocket } from 'phoenix_live_view'

import '../css/app.scss'

window.addEventListener('phx:page-loading-start', () => NProgress.start())
window.addEventListener('phx:page-loading-stop', () => NProgress.done())

const csrfToken = document
  .querySelector("meta[name='csrf-token']")
  .getAttribute('content')

const liveSocket = new LiveSocket('/live', Socket, {
  params: { _csrf_token: csrfToken },
})

liveSocket.connect()

window.liveSocket = liveSocket
