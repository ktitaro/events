defmodule Events.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :username, :string
      add :fullname, :string
      add :last_login, :utc_datetime
      add :is_online, :boolean, default: false, null: false
      add :is_deleted, :boolean, default: false, null: false
      add :deleted_at, :utc_datetime

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
